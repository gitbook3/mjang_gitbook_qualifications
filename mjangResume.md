Mike Jang, RHCE  
michael@linuxexam.com  
(503) 381-7368 (mobile)  


# SUMMARY
- Technical Writer focused on APIs, Security, User Experience, and Developer Experience. 
- Published author (20+ books). Experienced speaker. Linux specialist.

# RELEVANT WORK HISTORY
- Staff Technical Writer, Cobalt			        2021-2023.
- Senior Technical Writer, GitLab			        2019-2021.
- Senior Staff Technical Writer, ForgeRock			2012-2019.
- Self-employed Technical Author


# TECHNICAL EXPERTISE
- Static Site Generators: Hugo, Jekyll, and DocBook.
- Tools: Git, Jira, Confluence, IntelliJ, VS Code, GIMP, Maven, vi, SELinux, KVM.
- Certifications: Red Hat Certified Engineer (2002, 2006, 2010) plus others.
- Server applications: SSH, MySQL, Apache, Squid, Samba, vsFTP.
- Server hardware: configured/managed a server rack with VMware ESXi.
- Command line: everyday user of the Linux/Unix command line, including shell scripts.

# TECHNICAL WRITER
- Created readable security-focused product documentation using a docs-as-code workflow.
  - Built a foundation for a true Single Source of Truth. (Write once, publish everywhere.)
  - Result is easier to read than the competition. (Two grade levels easier, per Flesch-Kincaid.)
- Tested new features extensively to support better documentation.
  - Analyzed code in multiple programming languages for product functionality.
  - Created / tested sample code, REST calls, and Postman collections to aid deployment.
- Documented Identity and Access Management features, on-premises and in the cloud.
- Developed expertise in the following areas: Pentest methodologies, OAuth 2, OIDC, reconciliation, synchronization, authorization, and multi-factor authentication.
- Worked in Agile teams; strived to release documentation concurrently with development.
- Taught non-writers to create better UI text.
  - Created UI text style guides customized for corporate voice and tone.
  - Optimized UI text for developers, administrators, security professionals and end-users.


# COMMUNITY LEADER
- Global Lead, Write the Docs Meetups (2016-2020). Increased number of Meetups from 20 to 44.
- Founder, Write the Docs PDX Meetup (2014-2018). Built from 0 to over 900 members.
- Member, DevRel Collective, a private group of Developer Relations / Community Management professionals.

# CONFERENCE SPEAKER 
- UI Text, Simplicity is Difficult (Linuxconf Australia, 2021).
- When Linux on the Desktop is a Second Class Corporate Citizen (Open Source Summit, 2020).
- A Community That Meets Together Flourishes Together (Open Source Summit, 2019).
- How I Learned to Stop Worrying and Love the Command Line (Write the Docs, 2019).
- Minimum Viable Documentation for RESTful APIs (API Strategy Conference, 2018).
- Nurturing Global Meetups (Community Leadership Summit, 2018). 
- Setting Up the Integrated ForgeRock Platform (ForgeRock Identity Live UnSummit, 2018).
- UI Text: Simplicity is Difficult (OSCON 2017).
- Help Deployers Run Your Webapps (O’Reilly Fluent Conference, 2016).
- Start Your Own Write the Docs Meetup Group (Write the Docs Conference, 2015).
- Ten Steps to a Better README (OSCON Ignite, 2015).


# AUTHOR
- Wrote 25+ published books / video courses. 
- Specialized in Linux and the Red Hat Certified Engineer Exam.
- Taught Red Hat Certified Technician course at Portland Community College (CEU 9632).
- For a full list, see www.linkedin.com/in/mijang/details/publications/.

# EDUCATION
- Cornell University, B.S. Mechanical Engineering.

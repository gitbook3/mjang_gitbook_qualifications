# Mike Jang's Qualifications

Mike Jang's qualifications for the GitBook Product Specialist position.

- Resume: mjangResume_GitBook.docx
- Writing Samples: writingSamples.md
- Cover Letter: GitBook_cover_letter.docx
- Speaking Experience: speakingExperience.md


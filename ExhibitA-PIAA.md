## Exhibit A to the PIAA

The following is a complete list of all Works which have been made or conceived or first reduced to practice by me alone or jointly with others prior to my tenure with Company:

| Work (Title)                | ISBN  |
| ---------------------------|----------------------------------|
| RHCSA/RHCE Red Hat Linux Certification Study Guide, (7th Edition, 2014) | 0071841962 |
| RHCSA/RHCE Red Hat Linux Certification Study Guide, (6th Edition, 2011) | 0071765654 |
| Red Hat Certified Engineer Linux Study Guide, (5th Edition, 2007) | 0072264543 |
| Red Hat Certified Engineer Linux Study Guide, (4th Edition, 2004) | 0072253657 |
| Red Hat Certified Engineer Linux Study Guide, (3rd Edition, 2002) | 0072224851 |
| Oracle Solaris 11 System Administration, The Complete Reference (2012). | 007179042X | 
| Red Hat Linux Certification Practice Exams with Virtual Machines, (2nd Edition, 2020) | 007184208X | 
| Red Hat Linux Certification Practice Exams with Virtual Machines, (1st Edition, 2012) | 007180160X | 
| Security Strategies in Linux Platforms and Applications (2010) | 076379189X |
| Linux+ Video Course (2009) | 1-935320-91-2 | 
| Fedora 11 Video Course (2009) |  1-935320-67-X | 
| LPIC-1 In Depth (2009) | 1598639676 | 
| Ubuntu Server Administration Video Course (2009) | 1-935320-31-9 | 
| Ubuntu Certified Professional Video Course (2008) | 1-934743-97-6 | 
| Ubuntu Server Administration (2008) | 0071598928 | 
| Linux+ Passport (2008) | 0071546715 | 
| Red Hat Certified Engineer Video Course (2008) | 1-934743-47-X | 
| Red Hat Certified Technician Video Course (2007) | 1-933736-97-6 |
| Linux Annoyances for Geeks (2006) | 0596008015 | 
| Mastering Fedora Core 5 (2006) | 0470009993 |
| Linux Patch Management (2006) | 0132366754 |
| Mastering Red Hat Enterprise Linux 3 (2004) | 0782143474 | 
| Red Hat Certified Technician Study Guide (2004) | 0072255390 | 
| Linux Transfer for Windows Administrators (2003) | 1930919468 |
| Mastering Red Hat Linux 9 (2003) | 078214179X |
| Linux Networking Clearly Explained (2001) | 0125331711 |
| CompTIA Linux+ Exam Cram (2001) | 1588801942 |
| Sair Linux/GNU Installation and Configuration Exam Cram (2001) | 1576109534 | 
| Mastering Linux (2001) | 0782129153 |
| Exam Prep for Windows 98 (1998) | 1576102904 |
| A Guide to Microsoft Windows 98 (1998) | 0760010757 |


